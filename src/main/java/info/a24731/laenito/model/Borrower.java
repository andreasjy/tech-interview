package info.a24731.laenito.model;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Builder
public class Borrower {
  private Long id;

  @NotBlank(message = "First name is mandatory")
  private String firstName;

  @NotBlank(message = "Last name is mandatory")
  private String lastName;

  @NotBlank(message = "Document number is mandatory")
  private String documentNumber;

  //  @Pattern(regexp = "^(.+)@(.+)$", message = "Email ")
  @Email(message = "Email must have correct format")
  private String email;

  @NotBlank(message = "Address is mandatory")
  private String address;

  @NotNull(message = "Country is mandatory")
  private Long country;

  public static Borrower empty() {
    Borrower borrower = Borrower.builder()
      .firstName("")
      .lastName("")
      .address("")
      .country(0L)
      .documentNumber("")
      .email("")
      .build();

    return borrower;
  }
}
