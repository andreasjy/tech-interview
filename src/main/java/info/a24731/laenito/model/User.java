package info.a24731.laenito.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class User {
  private String userName;
  private String password;
  private String roles;
}
