package info.a24731.laenito.model;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
@Builder
public class Loan {
  private Long id;

  private Long borrowerId;
  private String owner;

  @NotNull(message = "Principal is mandatory")
  @Positive(message = "Principal must be positive amount")
  private Double principal;

  @NotNull(message = "Interest is mandatory")
  @Positive(message = "Interest must be positive")
  private Double interest;
  private Double total;

  public static Loan empty() {
    Loan loan = Loan.builder()
      .owner("")
      .borrowerId(0L)
      .principal(0.0)
      .interest(0.0)
      .build();

    return loan;
  }
}
