package info.a24731.laenito.controller;

import info.a24731.laenito.dto.LoanInputFormDTO;
import info.a24731.laenito.model.Loan;
import info.a24731.laenito.service.CountryService;
import info.a24731.laenito.service.LoanService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.List;

import static java.util.Collections.emptyList;

@Slf4j
@Controller
public class LoanController {

  private final LoanService loanService;
  private final CountryService countryService;

  @Autowired
  public LoanController(LoanService loanService, CountryService countryService) {
    this.loanService = loanService;
    this.countryService = countryService;
  }

  @GetMapping("/")
  public String home() {
    return "redirect:/list";
  }

  @GetMapping("loan")
  public String loan(@RequestParam(required = false, name = "id") Long id, Model model) {
    String username = getUsername();
    if (username != null) {
      model.addAttribute("countries", countryService.getCountries());
      model.addAttribute("dto", (id == null)? LoanInputFormDTO.empty() : loanService.getLoan(id, username));
    }

    return "loan";
  }

  @PostMapping("save")
  public String save(@Valid @ModelAttribute("dto") LoanInputFormDTO dto, BindingResult result, Model model) {
    if (result.hasErrors()) {
      model.addAttribute("countries", countryService.getCountries());
      model.addAttribute("dto", dto);

      return "loan";
    }

    String username = getUsername();
    if (username != null) {
      dto.getLoan().setOwner(username);

      if (dto.getLoan().getId() == null)
        loanService.saveLoan(dto);
      else
        loanService.updateLoan(dto);
    }

    return "redirect:/list";
  }

  @GetMapping("list")
  public String list(Model model) {
    String username = getUsername();
    if (username != null) {
      List<Loan> list = loanService.getLoans(username);
      model.addAttribute("loans", list);
    } else {
      model.addAttribute("loans", emptyList());
    }

    return "list";
  }

  private String getUsername() {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    if (!(authentication instanceof AnonymousAuthenticationToken)) {
      UserDetails userPrincipal = (UserDetails) authentication.getPrincipal();
      return userPrincipal.getUsername();
    }
    return null;
  }
}
