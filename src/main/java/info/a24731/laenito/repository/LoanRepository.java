package info.a24731.laenito.repository;

import info.a24731.laenito.model.Loan;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Repository
public class LoanRepository {
  private final JdbcTemplate jdbcTemplate;
  private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

  @Autowired
  public LoanRepository(NamedParameterJdbcTemplate namedParameterJdbcTemplate, JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
    this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
  }

  public Long createLoan(Loan loan) {
    SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(jdbcTemplate);
    simpleJdbcInsert.withTableName("loan");
    simpleJdbcInsert.usingGeneratedKeyColumns("id");

    return simpleJdbcInsert.executeAndReturnKey(toMap(loan)).longValue();
  }

  private Map<String, Object> toMap(Loan loan) {
    Map<String, Object> ret = new HashMap<>();
    ret.put("fk_owner", loan.getOwner());
    ret.put("fk_borrower_id", loan.getBorrowerId());
    ret.put("interest", loan.getInterest());
    ret.put("principal", loan.getPrincipal());
    return ret;
  }

  public Loan getLoan(Long id, String owner) {
    String query = "select id, fk_owner, fk_borrower_id, principal, interest, principal + (principal*interest/100) as total from loan where id = :id and fk_owner = :owner";
    return namedParameterJdbcTemplate.queryForObject(query, new MapSqlParameterSource("id", id).addValue("owner", owner), this::mapRowToLoan);
  }

  public void updateLoan(Loan loan) {
    String query = "update loan set fk_borrower_id = :borrower, principal = :principal, interest = :interest where id = :id and fk_owner = :owner";

    namedParameterJdbcTemplate.update(query,
      new MapSqlParameterSource("id", loan.getId())
        .addValue("owner", loan.getOwner())
        .addValue("borrower", loan.getBorrowerId())
        .addValue("principal", loan.getPrincipal())
        .addValue("interest", loan.getInterest())
    );
  }

  public List<Loan> getLoans(String owner) {
    String query = "select id, fk_owner, fk_borrower_id, principal, interest, principal + (principal*interest/100) as total from loan where fk_owner = :owner";
    return namedParameterJdbcTemplate.query(query, new MapSqlParameterSource("owner", owner), this::mapRowToLoan);
  }

  private Loan mapRowToLoan(ResultSet rs, int rowNum) throws SQLException {
    return Loan.builder()
      .id(rs.getLong("id"))
      .principal(rs.getDouble("principal"))
      .interest(rs.getDouble("interest"))
      .total(rs.getDouble("total"))
      .borrowerId(rs.getLong("fk_borrower_id"))
      .owner(rs.getString("fk_owner"))
      .build();
  }

  @Transactional
  public void deleteAll() {
    String query = "delete from loan";
    jdbcTemplate.execute(query);
  }
}
