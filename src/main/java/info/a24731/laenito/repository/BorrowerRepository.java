package info.a24731.laenito.repository;

import info.a24731.laenito.model.Borrower;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Repository
public class BorrowerRepository {
  private final JdbcTemplate jdbcTemplate;
  private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

  @Autowired
  public BorrowerRepository(JdbcTemplate jdbcTemplate, NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
    this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
  }

  public Long createBorrower(Borrower borrower) {
    SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(jdbcTemplate);
    simpleJdbcInsert.withTableName("borrower");
    simpleJdbcInsert.usingGeneratedKeyColumns("id");

    return simpleJdbcInsert.executeAndReturnKey(toMap(borrower)).longValue();
  }

  private Map<String, Object> toMap(Borrower borrower) {
    Map<String, Object> ret = new HashMap<>();
    ret.put("first_name", borrower.getFirstName());
    ret.put("last_name", borrower.getLastName());
    ret.put("email", borrower.getEmail());
    ret.put("document_number", borrower.getDocumentNumber());
    ret.put("address", borrower.getAddress());
    ret.put("fk_country", borrower.getCountry());
    return ret;
  }

  public Borrower getBorrower(Long id) {
    String query = "select id, first_name, last_name, document_number, email, address, fk_country from borrower where id = :id";
    return namedParameterJdbcTemplate.queryForObject(query, new MapSqlParameterSource("id", id), this::mapRowToBorrower);
  }

  public void updateBorrower(Borrower borrower) {
    String query = "update borrower set first_name = :first_name , last_name = :last_name, document_number = :document_number, email = :email, address = :address, fk_country = :fk_country where id = :id";

    namedParameterJdbcTemplate.update(query,
      new MapSqlParameterSource("id", borrower.getId())
        .addValue("first_name", borrower.getFirstName())
        .addValue("last_name", borrower.getLastName())
        .addValue("document_number", borrower.getDocumentNumber())
        .addValue("email", borrower.getEmail())
        .addValue("address", borrower.getAddress())
        .addValue("fk_country", borrower.getCountry())
    );
  }

  private Borrower mapRowToBorrower(ResultSet rs, int rowNum) throws SQLException {
    return Borrower.builder()
      .id(rs.getLong("id"))
      .firstName(rs.getString("first_name"))
      .lastName(rs.getString("last_name"))
      .documentNumber(rs.getString("document_number"))
      .email(rs.getString("email"))
      .address(rs.getString("address"))
      .country(rs.getLong("fk_country"))
      .build();
  }

  public void deleteAll() {
    String query = "delete from borrower";
    jdbcTemplate.execute(query);
  }
}
