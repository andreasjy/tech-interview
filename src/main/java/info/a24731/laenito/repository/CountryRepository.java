package info.a24731.laenito.repository;

import info.a24731.laenito.model.Country;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Slf4j
@Repository
public class CountryRepository {
  private final JdbcTemplate template;

  @Autowired
  public CountryRepository(JdbcTemplate jdbcTemplate) {
    template = jdbcTemplate;
  }

  public List<Country> getCountries() {
    String query = "select iso_numeric_code, name, iso_alpha_2_code from country";
    return template.query(query, this::mapRowToCountry);
  }

  private Country mapRowToCountry(ResultSet rs, int rowNum) throws SQLException {
    return Country.builder()
      .iso_numeric_code(rs.getLong("iso_numeric_code"))
      .name(rs.getString("name"))
      .iso_alpha_2_code(rs.getString("iso_alpha_2_code"))
      .build();
  }
}
