package info.a24731.laenito.service;

import info.a24731.laenito.dto.LoanInputFormDTO;
import info.a24731.laenito.model.Borrower;
import info.a24731.laenito.model.Loan;
import info.a24731.laenito.repository.BorrowerRepository;
import info.a24731.laenito.repository.LoanRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Slf4j
@Service
public class LoanService {

  private final LoanRepository loanRepository;
  private final BorrowerRepository borrowerRepository;

  @Autowired
  public LoanService(LoanRepository repoLoan, BorrowerRepository repoBorrower) {
    this.loanRepository = repoLoan;
    this.borrowerRepository = repoBorrower;
  }

  @Transactional
  public LoanInputFormDTO getLoan(Long id, String owner) {
    Loan loan = loanRepository.getLoan(id, owner);
    Borrower borrower = borrowerRepository.getBorrower(loan.getBorrowerId());

    return LoanInputFormDTO.builder().loan(loan).borrower(borrower).build();
  }

  @Transactional
  public List<Loan> getLoans(String username) {
    return loanRepository.getLoans(username);
  }

  @Transactional
  public Long saveLoan(LoanInputFormDTO dto) {
    Long borrowerId = borrowerRepository.createBorrower(dto.getBorrower());
    dto.getLoan().setBorrowerId(borrowerId);
    Long loanId = loanRepository.createLoan(dto.getLoan());
    log.debug("created loan {}", loanId);
    return loanId;
  }

  @Transactional
  public void updateLoan(LoanInputFormDTO dto) {
    borrowerRepository.updateBorrower(dto.getBorrower());
    loanRepository.updateLoan(dto.getLoan());
  }

  @Transactional
  public void deleteAll() {
    loanRepository.deleteAll();
    borrowerRepository.deleteAll();
  }
}
