package info.a24731.laenito.service;

import info.a24731.laenito.model.Country;
import info.a24731.laenito.repository.CountryRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Slf4j
@Service
public class CountryService {
  private final CountryRepository countryRepository;

  public CountryService(CountryRepository countryRepository) {
    this.countryRepository = countryRepository;
  }

  @Transactional
  public List<Country> getCountries() {
    return countryRepository.getCountries();
  }
}
