package info.a24731.laenito.service;

import info.a24731.laenito.dto.LoanInputFormDTO;
import info.a24731.laenito.model.Borrower;
import info.a24731.laenito.model.Country;
import info.a24731.laenito.model.Loan;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@SpringBootTest
public class LoanServiceTest {

  @Autowired
  private LoanService svc;

  @Test
  public void readLoan() {
    LoanInputFormDTO dto = svc.getLoan(1508L, "santa");
    assertNotNull(dto);
  }

  @Test
  public void createLoan() {
    Country country = Country.builder()
      .iso_numeric_code(246L)
      .build();

    Borrower borrower = Borrower.builder()
      .firstName("Keegi")
      .lastName("Eikeegi")
      .address("Helsingis")
      .country(country.getIso_numeric_code())
      .documentNumber("Soome ID-kaart")
      .email("any@where.net")
      .build();

    Loan loan = Loan.builder()
      .owner("santa")
      .borrowerId(borrower.getId())
      .principal(50.0)
      .interest(5.0)
      .build();

    LoanInputFormDTO dto = LoanInputFormDTO.builder()
      .loan(loan)
      .borrower(borrower)
      .build();

    Long id = svc.saveLoan(dto);

    LoanInputFormDTO found = svc.getLoan(id, "santa");
    assertNotNull(found);
  }

  @Test
  public void saveLoan() {
    List<Loan> santa = svc.getLoans("santa");

    if (santa.isEmpty())
      fail();

    Loan loan = santa.get(0);

    Country country = Country.builder()
      .iso_numeric_code(246L)
      .build();

    Borrower borrower = Borrower.builder()
      .id(loan.getBorrowerId())
      .firstName("Keegi")
      .lastName("Eikeegi")
      .address("Helsingis")
      .country(country.getIso_numeric_code())
      .documentNumber("Soome ID-kaart 100")
      .email("keegi@soomes.fi")
      .build();

    Double old = loan.getPrincipal();
    Double principal = old + 10000.00;
    loan.setPrincipal(principal);

    LoanInputFormDTO dto = LoanInputFormDTO.builder()
      .loan(loan)
      .borrower(borrower)
      .build();

    svc.updateLoan(dto);

    LoanInputFormDTO found = svc.getLoan(loan.getId(), "santa");
    assertNotNull(found);
    assertEquals(principal, found.getLoan().getPrincipal(), 0.0001);
  }
}
